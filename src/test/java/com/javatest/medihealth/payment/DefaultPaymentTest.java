package com.javatest.medihealth.payment;


import com.javatest.medihealth.model.Patient;
import com.javatest.medihealth.model.Service;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.javatest.medihealth.model.Service.BLOOD_TEST;
import static com.javatest.medihealth.model.Service.DIAGNOSIS;
import static com.javatest.medihealth.model.Service.ECG;
import static com.javatest.medihealth.model.Service.VACCINE;
import static com.javatest.medihealth.model.Service.XRAY;
import static java.math.BigDecimal.*;
import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPaymentTest {

    private DefaultPayment unit;

    @Before
    public void setUp(){
        unit = new DefaultPayment();
    }


    @Test
    public void shouldReturnPriceWithoutDiscountWhenCalculatePriceWithPatientWithoutAnyDiscountCondition(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(25));
        patient.setHealthInsurance(false);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, XRAY, BLOOD_TEST, ECG, VACCINE));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(530.9)));
    }

    @Test
    public void shouldCalculateVaccineServicePriceOnceWhenCalculatePriceWithTwoVaccine(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(25));
        patient.setHealthInsurance(false);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(VACCINE, VACCINE));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(57.5)));
    }

    @Test
    public void shouldReturn60PercentDiscountWhenCalculatePriceWithPatientBetween65and70(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(65));
        patient.setHealthInsurance(false);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, XRAY, BLOOD_TEST, ECG, VACCINE));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(212.36)));
    }

    @Test
    public void shouldReturn90PercentDiscountWhenCalculatePriceWithPatientOver70(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(70));
        patient.setHealthInsurance(false);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, XRAY, BLOOD_TEST, ECG, VACCINE));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(53.09)));
    }

    @Test
    public void shouldReturn40PercentDiscountWhenCalculatePriceWithPatientUnder5(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(4));
        patient.setHealthInsurance(false);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, XRAY, BLOOD_TEST, ECG, VACCINE));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(318.54)));
    }

    @Test
    public void shouldReturn15PercentDiscountOnBloodTestWhenCalculatePriceWithDiagnosisAndBloodTestAndPatientWithHealthInsurance(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(25));
        patient.setHealthInsurance(true);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, BLOOD_TEST));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(126.3)));
    }

    @Test
    public void shouldReturn15PercentDiscountOnBloodTestAnd60PercentOverallDiscountWhenCalculatePriceWithDiagnosisAndBloodTestAndPatientWithHealthInsuranceBetween65And70(){
        // GIVEN
        Patient patient = new Patient();
        patient.setDateOfBirth(LocalDate.now().minusYears(65));
        patient.setHealthInsurance(true);

        List<Service> serviceList = new ArrayList<>();
        serviceList.addAll(asList(DIAGNOSIS, BLOOD_TEST));
        // WHEN
        BigDecimal result = unit.calculatePrice(patient, serviceList);
        // THEN
        assertThat(result, is(valueOf(50.52)));
    }
}