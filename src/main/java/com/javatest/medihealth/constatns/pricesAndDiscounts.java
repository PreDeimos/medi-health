package com.javatest.medihealth.constatns;

public class pricesAndDiscounts {
    public static Double DIAGNOSIS_PRICE = 60.0;
    public static Double XRAY_PRICE = 150.0;
    public static Double BLOOD_TEST_PRICE = 78.0;
    public static Double ECG_PRICE = 200.40;
    public static Double VACCINE_SERVICE_PRICE = 27.5;
    public static Double VACCINE_PRICE = 15.0;

    public static Double BETWEEN65AND70_DISCOUNT = 60.0;
    public static Double AFTER70_DISCOUNT = 90.0;
    public static Double UNDER5_DISCOUNT = 40.0;
    public static Double HEALTH_INSURANCE_DISCOUNT = 15.0;
}
