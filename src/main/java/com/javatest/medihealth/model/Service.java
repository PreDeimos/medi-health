package com.javatest.medihealth.model;

public enum Service {
    DIAGNOSIS,
    XRAY,
    BLOOD_TEST,
    ECG,
    VACCINE;
}
