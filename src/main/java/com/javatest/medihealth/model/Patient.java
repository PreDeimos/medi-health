package com.javatest.medihealth.model;

import java.time.LocalDate;

public class Patient {

    private String Name;

    private LocalDate DateOfBirth;

    private Boolean HealthInsurance;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public LocalDate getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public Boolean getHealthInsurance() {
        return HealthInsurance;
    }

    public void setHealthInsurance(Boolean healthInsurance) {
        HealthInsurance = healthInsurance;
    }
}
