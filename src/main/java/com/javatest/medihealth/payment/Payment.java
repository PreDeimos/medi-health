package com.javatest.medihealth.payment;

import com.javatest.medihealth.model.Patient;
import com.javatest.medihealth.model.Service;

import java.math.BigDecimal;
import java.util.List;

public interface Payment {

    BigDecimal calculatePrice(Patient patient, List<Service> serviceList);

}
