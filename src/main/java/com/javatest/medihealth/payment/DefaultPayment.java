package com.javatest.medihealth.payment;

import com.javatest.medihealth.model.Patient;
import com.javatest.medihealth.model.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import static com.javatest.medihealth.constatns.pricesAndDiscounts.AFTER70_DISCOUNT;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.BETWEEN65AND70_DISCOUNT;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.BLOOD_TEST_PRICE;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.DIAGNOSIS_PRICE;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.ECG_PRICE;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.HEALTH_INSURANCE_DISCOUNT;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.UNDER5_DISCOUNT;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.VACCINE_PRICE;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.VACCINE_SERVICE_PRICE;
import static com.javatest.medihealth.constatns.pricesAndDiscounts.XRAY_PRICE;
import static com.javatest.medihealth.model.Service.*;
import static java.math.BigDecimal.*;
import static java.util.Arrays.*;

public class DefaultPayment implements Payment{

    public BigDecimal calculatePrice(Patient patient, List<Service> serviceList) {
        BigDecimal totalPrice = ZERO;
        boolean vaccineUsed = false;
        for(Service service : serviceList){
            Double price = getPriceOfService(service, vaccineUsed);

            totalPrice = totalPrice.add(valueOf(price));
            vaccineUsed = vaccineUsed || service == VACCINE;
        }
        totalPrice = calculateDiscount(totalPrice, patient, serviceList);
        return totalPrice.stripTrailingZeros();
    }

    private Double getPriceOfService(Service service, boolean vaccineAlreadyUsed){
        Double price = 0.0;
        switch (service) {
            case DIAGNOSIS:
                price = DIAGNOSIS_PRICE;
                break;
            case XRAY:
                price = XRAY_PRICE;
                break;
            case BLOOD_TEST:
                price = BLOOD_TEST_PRICE;
                break;
            case ECG:
                price = ECG_PRICE;
                break;
            case VACCINE:
                price = VACCINE_PRICE + (vaccineAlreadyUsed ? 0.0 : VACCINE_SERVICE_PRICE);
                break;
        }
        return price;
    }

    private BigDecimal calculateDiscount(BigDecimal totalPrice, Patient patient, List<Service> serviceList){
        int patientAge = Period.between(patient.getDateOfBirth(), LocalDate.now()).getYears();
        if (patient.getHealthInsurance() && serviceList.containsAll(asList(DIAGNOSIS, BLOOD_TEST))){
            BigDecimal discount = valueOf(getPriceOfService(BLOOD_TEST, false)).multiply(getInvertDiscountMultiply(HEALTH_INSURANCE_DISCOUNT));

            totalPrice = totalPrice.subtract(discount);
        }
        if (patientAge >= 65 && patientAge < 70){
            totalPrice = totalPrice.multiply(getDiscountMultiply(BETWEEN65AND70_DISCOUNT));
        } else if (patientAge >= 70) {
            totalPrice = totalPrice.multiply(getDiscountMultiply(AFTER70_DISCOUNT));
        } else if (patientAge < 5){
            totalPrice = totalPrice.multiply(getDiscountMultiply(UNDER5_DISCOUNT));
        }
        return totalPrice;
    }

    private BigDecimal getDiscountMultiply(Double discountPercentage){
        return valueOf((100 - discountPercentage)/100);
    }

    private BigDecimal getInvertDiscountMultiply(Double discountPercentage){
        return valueOf(discountPercentage/100);
    }
}
